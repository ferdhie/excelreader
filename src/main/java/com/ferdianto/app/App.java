package com.ferdianto.app;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.*;

import javax.swing.*;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableModel;
import java.awt.*;
import java.io.IOException;
import java.io.InputStream;

/**
 * Hello world!
 *
 */
public class App 
{
    class ExcelSheetTableModel extends AbstractTableModel {

        private Sheet sheet;
        private int rowCount;
        private int columnCount;

        public ExcelSheetTableModel(Sheet sheet) {
            this.sheet=sheet;
            int maxCell = 0;
            for(int index=sheet.getFirstRowNum(), end = sheet.getLastRowNum(); index<=end; index++) {
                Row row = sheet.getRow(index);
                maxCell = Math.max(row.getLastCellNum(), maxCell);
            }
            columnCount = maxCell;
            rowCount = sheet.getLastRowNum();
        }

        public String getSheetName() {
            return sheet.getSheetName();
        }

        @Override
        public int getRowCount() {
            return rowCount;
        }

        @Override
        public int getColumnCount() {
            return columnCount;
        }

        @Override
        public Object getValueAt(int rowIndex, int columnIndex) {
            Cell cell = sheet.getRow(rowIndex).getCell(columnIndex);
            if (cell.getCellTypeEnum() == CellType.NUMERIC) {
                return cell.getNumericCellValue();
            } else {
                return cell.getStringCellValue();
            }
        }
    }


    public JFrame readFile() throws IOException {

        InputStream in = App.class.getClassLoader().getResourceAsStream("brilliant-baby-names.xls");
        //kalau xlsx XSSFWorkbook
        Workbook workbook = new HSSFWorkbook(in);
        int numberOfSheets = workbook.getNumberOfSheets();

        JTabbedPane tabbedPane = new JTabbedPane();
        tabbedPane.setTabPlacement(JTabbedPane.BOTTOM);

        for (int i = 0; i < numberOfSheets; i++) {
            Sheet sheet = workbook.getSheetAt(i);
            ExcelSheetTableModel tableModel = new ExcelSheetTableModel(sheet);
            JTable table = new JTable(tableModel);
            tabbedPane.addTab(tableModel.getSheetName(), new JScrollPane(table));
        }

        JFrame frame = new JFrame("brilliant-baby-names.xls");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        frame.setLayout(new BorderLayout());
        frame.add(tabbedPane, BorderLayout.CENTER);

        return frame;
    }

    public static void main( String[] args ) throws IOException {
        App app = new App();
        final JFrame window = app.readFile();
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                window.pack();
                window.setVisible(true);
            }
        });
    }
}
